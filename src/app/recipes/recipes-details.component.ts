import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecipeService } from './recipe.service';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from './recipe.model';
import { catchError } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-recipes-details',
  templateUrl: './recipes-details.component.html',
  styleUrls: ['./recipes-details.component.css']
})
export class RecipesDetailsComponent implements OnInit, OnDestroy {
  recipe: Recipe;
  private sub: Subscription;

  constructor(
    private service: RecipeService,
    private route: ActivatedRoute) { }

    ngOnInit(): void {
      const id = this.route.snapshot.params.id;

      this.sub = this.service.getRecipe(id).subscribe(
        result => this.recipe = result,
        err => catchError(err)
        );
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }

}
