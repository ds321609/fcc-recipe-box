import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import {
  RecipesFormComponent,
  RecipesListComponent
} from './';
import { RecipesDetailsComponent } from './recipes-details.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { RecipesDeleteDialogComponent } from './recipes-delete-dialog/recipes-delete-dialog.component';
import { ShortenTextPipe } from '../shared/shorten-text.pipe';


@NgModule({
  declarations: [
    RecipesFormComponent,
    RecipesListComponent,
    RecipesDetailsComponent,
    RecipesDeleteDialogComponent,
  ],
  imports: [
    // FormsModule,
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatIconModule,
    RouterModule,
    MatListModule
  ],
  entryComponents: [
    RecipesDeleteDialogComponent
  ]
})
export class RecipesModule { }

