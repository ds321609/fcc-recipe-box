import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Recipe } from './recipe.model';
import { ObserveOnMessage } from 'rxjs/internal/operators/observeOn';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor() {
    if (!localStorage.getItem('recipeList')) {
      localStorage.setItem('recipeList', JSON.stringify([]));
    }
  }

  private handleError(err): Observable<object> {
    let errorMessage: string;

    errorMessage = `An error occurred: ${err}`;

    console.error(err);
    return throwError(errorMessage);
  }

  getAllRecipes(): Observable<Recipe[]> {
    try {
      const stringifiedList = localStorage.getItem('recipeList');
      return  of(JSON.parse(stringifiedList));
    } catch (err) {
      this.handleError(err);
    }
  }

  getRecipe(id): Observable<Recipe> {
    const parsedList = this.cloneLocalList();

    try {
      const recipe: Recipe = parsedList.find((item: { id: any; }) => item.id === id);

      if (id === '0') {
        const blank = {
          id: '0',
          name: '',
          description: '',
          ingredients: []
        };

        return of(blank);
      }

      if (recipe) {
        return of(recipe);
      }

      throw new Error('That recipe cannot be found!');
    } catch (err) {
      this.handleError(err);
    }
  }

  updateRecipe(recipe: Recipe): Observable<object> {
    const parsedList = this.cloneLocalList();
    const updatedList = parsedList.map(item => recipe.id === item.id ? recipe : item);

    this.saveLocalStorage(updatedList);
    return of({ success: true});
  }

  private saveLocalStorage(updatedList: any) {
    localStorage.setItem('recipeList', JSON.stringify(updatedList));
  }

  postRecipe(recipe: Recipe): Observable<object> {
    const indexedRecipe = Object.assign(recipe, { id: uuidv4() });
    const parsedList = this.cloneLocalList();
    try {
      const newRecipe = this.buildRecipe(indexedRecipe);
      const updatedList = [ ...parsedList, newRecipe];
      localStorage.setItem('recipeList', JSON.stringify(updatedList));
      return of({ status: 'success' });
    } catch (err) {
      this.handleError(err);
    }
  }

  deleteRecipe(id: string): Observable<object> {
    const parsedList = this.cloneLocalList();

    try {
      const updatedList = parsedList.filter(item => item.id !== id);
      this.saveLocalStorage(updatedList);

      return of({ success: true });
    } catch (err) {
      this.handleError(err);
    }
  }

  private buildRecipe(indexedRecipe: Recipe & { id: any; }) {
    return Object.assign({}, { id: indexedRecipe, ...indexedRecipe });
  }

  private cloneLocalList() {
    return JSON.parse(localStorage.getItem('recipeList'));
  }
}

