import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { RecipeService } from './recipe.service';
import { Recipe } from './recipe.model';
import { catchError } from 'rxjs/operators';
import { RecipesDeleteDialogComponent } from './recipes-delete-dialog/recipes-delete-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'description', 'actions'];
  dataSource: Recipe[];
  dialogSub: Subscription;
  listingSub: Subscription;

  @HostListener('window:resize', ['$event'])
  onResize() {
    const mobile = window.innerWidth <= 600;

    if(mobile) {
      this.displayedColumns = ['name', 'actions'];
    } else {
      this.displayedColumns = ['name', 'description', 'actions'];
    }
  }

  constructor(
    private toastr: ToastrService,
    private service: RecipeService,
    private router: Router,
    public dialog: MatDialog) { }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(RecipesDeleteDialogComponent, {
      data: {
        name: this.dataSource.find(recipe => recipe.id === id).name,
        id
      }
    });

    this.dialogSub = dialogRef.afterClosed().subscribe(result => {
      const deleted = result.isDeleted;
      console.log(result);

      if (deleted) {
        this.retriveRecipesListing();
        this.toastr.info(`Recipe has been deleted!`);
      }
    });
  }

  ngOnInit(): void {
    this.retriveRecipesListing();
    this.onResize();
  }

  ngOnDestroy(): void {
    this.listingSub.unsubscribe();
    this.dialogSub?.unsubscribe();
  }

  getDetails(id: string): void {
    this.router.navigate(['/recipe-details', id]);
  }

  retriveRecipesListing(): void {
    this.listingSub = this.service.getAllRecipes().subscribe(
      recipes => this.dataSource = recipes,
      catchError(err => err)
    );
  }
}
