import { Component, OnInit, AfterViewInit, ElementRef, ViewChildren, OnDestroy, QueryList, ChangeDetectorRef, } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RecipeService } from './recipe.service';
import {FormControl, Validators, FormGroup, FormBuilder, FormControlName, FormArray} from '@angular/forms';
import { Observable, fromEvent, merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { GenericValidator } from '../shared/generic-validator';
import { Recipe } from './recipe.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-recipes-form',
  templateUrl: './recipes-form.component.html',
  styleUrls: ['./recipes-form.component.css']
})
export class RecipesFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
  @ViewChildren('ingredientElems') ingredientElems: QueryList<ElementRef>;

  viewTitle: string;

  displayMessage: { [key: string]: string } = {};
  recipeForm: FormGroup;
  validationMessages: { [key: string]: { [key: string]: string }};
  private genericValidator: GenericValidator;

  recipe: Recipe;
  private sub: Subscription;

  get ingredients(): FormArray {
    return this.recipeForm.get('ingredients') as FormArray;
  }

  constructor(private toastr: ToastrService,
              private service: RecipeService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef)
  {
    this.validationMessages = {
      name: {
        required: 'Please provide a name for the recipe',
        minlength: 'Name needs to be more than one character',
        maxlength: 'Name needs to be less than or equal to 50 characters'
      },
      description: {
        maxlength: 'The description limit is 500 characters',
        minlength: 'The minimal amount of characters for the description is 3'
      },
    };

    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngOnInit(): void {
    this.recipeForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      description: ['', [Validators.minLength(3), Validators.maxLength(500)]],
      ingredients: this.fb.array([])
    });

    this.addIngredient();

    this.sub = this.route.paramMap.subscribe(
      params => {
        const id = params.get('id');
        this.getRecipe(id);
      }
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  getRecipe(id: string) {
    this.service.getRecipe(id).subscribe({
      next: (recipe: Recipe) => this.displayRecipe(recipe),
      error: err => console.error(err)
    });
  }
  displayRecipe(recipe: Recipe): void {
    this.recipe = recipe;

    if (recipe.id === '0') {
      this.viewTitle = 'Add Recipe';
    } else {
      this.viewTitle = `Edit Recipe: ${this.recipe.name}`;
      this.recipeForm.patchValue({
        name: this.recipe.name,
        description: this.recipe.description,
      });
      this.recipeForm.setControl('ingredients', this.fb.array(this.recipe.ingredients || []));
    }

  }

  ngAfterViewInit(): void {
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

    merge(this.recipeForm.valueChanges, ...controlBlurs).pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.recipeForm);
    });
  }

  addIngredient() {
    const valid = this.recipeForm.get('ingredients').valid;

    if (valid) {
      this.ingredients.push(new FormControl('', Validators.required));
    }

    if(this.ingredients.length > 1) {
      this.cd.detectChanges();
      console.log(this.ingredientElems);
      this.ingredientElems.last.nativeElement.focus();
    }

    return false;
  }

  canSave() {
    const form = this.recipeForm;
    const valid = form.valid && form.dirty;

    return !valid && !this.lastIngredientIsValid();
  }

  saveRecipe() {
    if (this.recipeForm.valid) {
      if (this.recipeForm.dirty) {
        const r = { ...this.recipe, ...this.recipeForm.value };

        if (this.recipe.id === '0') {
          this.service.postRecipe(r).subscribe(
            result => {
              this.toastr.success('success!');
              this.router.navigate(['']);
            },
            error => {
              this.toastr.error(`Something went wrong when creating recipe: ${error}`);
              console.error(`Failed to add recipe: \n ${error}`)
            }
          );
        } else {
          this.service.updateRecipe(r).subscribe(
            result => {
              this.toastr.success('success!');
              this.router.navigate(['']);
            },
            error => {
              this.toastr.error(`Something went wrong with the update: ${error}`);
              console.error(`Failed to update recipe: \n ${error}`)
            }
          );
        }
      }
    }
  }

  private lastIngredientIsValid() {
    const ingredients = this.ingredients.controls;
    const lastIngredient = ingredients[ingredients.length - 1];

    const empty = lastIngredient.value.length === 0;
    const notFirst = ingredients.length > 1;
    const notTouched = !lastIngredient.touched;

    return (empty && notFirst && notTouched);
  }
}

