import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesDeleteDialogComponent } from './recipes-delete-dialog.component';

describe('RecipesDeleteDialogComponent', () => {
  let component: RecipesDeleteDialogComponent;
  let fixture: ComponentFixture<RecipesDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipesDeleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
