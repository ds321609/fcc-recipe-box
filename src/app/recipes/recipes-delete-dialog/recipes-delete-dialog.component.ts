import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecipeService } from '../recipe.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-recipes-delete-dialog',
  templateUrl: './recipes-delete-dialog.component.html',
  styleUrls: ['./recipes-delete-dialog.component.css']
})
export class RecipesDeleteDialogComponent implements OnInit {

  constructor(
    private service: RecipeService,
    public dialogRef: MatDialogRef<RecipesDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  deleteRecipe(id: string): void {
    this.service.deleteRecipe(id).subscribe(
      result => this.dialogRef.close({ result, isDeleted: true }),
      catchError(async err => {
        return await this.dialogRef.close({ err, isDeleted: false });
      })
    );

  }

  cancelDelete(): void {
    this.dialogRef.close({ isDeleted: false });
  }

}
