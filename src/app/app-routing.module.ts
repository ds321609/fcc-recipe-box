import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  RecipesFormComponent,
  RecipesListComponent
} from './recipes';
import { RecipesDetailsComponent } from './recipes/recipes-details.component';

const routes: Routes = [
  {
    path: '',
    component: RecipesListComponent
  },
  {
    path: 'recipe-details/:id',
    component: RecipesDetailsComponent
  },
  {
    path: 'recipe-form/:id/edit',
    component: RecipesFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
