import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-nav',
  templateUrl: './recipe-nav.component.html',
  styleUrls: ['./recipe-nav.component.css']
})
export class RecipeNavComponent implements OnInit {

  active = 1;

  constructor() { }

  ngOnInit(): void {
  }

}
