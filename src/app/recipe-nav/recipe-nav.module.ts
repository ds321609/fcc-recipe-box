import { NgModule } from '@angular/core';
import { RecipeNavComponent } from './recipe-nav.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    RecipeNavComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    RecipeNavComponent
  ]
})
export class RecipeNavModule { }
