import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortenText'
})
export class ShortenTextPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    const strArray = value.split(' ');
    let shortenStr: string[] = [];

    if(strArray.length <= 30) { return value;}

    for (let i = 0; i < 30; i++) {
      shortenStr = [...shortenStr, strArray[i]];
    }
    console.log(shortenStr);
    return shortenStr.join(' ').concat('...');
  }

}
