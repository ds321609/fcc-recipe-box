import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericValidator } from './generic-validator';
import { FormsModule } from '@angular/forms';
import { ShortenTextPipe } from './shorten-text.pipe';



@NgModule({
  declarations: [
    ShortenTextPipe,
  ],
  imports: [
    CommonModule,

  ],
  exports: [
    CommonModule,
    FormsModule,
    ShortenTextPipe
  ]

})
export class SharedModule { }
